import 'package:flutter/material.dart';
import 'package:flutter_trainning/Day5/day5.dart';
class Webview extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(),
      body: Center(
        child: TextButton(
          onPressed: (){
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Day5(url: 'https://flutter.dev')));
          },
          child: Text('Click'),
        ),
      ),
    );
  }
}
