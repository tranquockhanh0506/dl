import 'package:flutter/material.dart';
import 'package:flutter_trainning/Day1/day1.dart';
import 'package:flutter_trainning/Day2/day2.dart';
import 'package:flutter_trainning/Day3/day3.dart';
class Butonavigation extends StatefulWidget {
  @override
  State<Butonavigation> createState() => _MyStatefulWidgetState();
}
class _MyStatefulWidgetState extends State<Butonavigation> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
List<Widget> _widgetOptions = <Widget>[
Day1(),
  Day2(),
  Day3(),
  ];
int coloricon = 0;
int clindex1=1;
int clindex2 =2;
int clindex3 =3;
//
int cliconindex1=0xFFB56E3F;
int cliconindex2 =0xFFB56E3F;
int cliconindex3 =0xFFB56E3F;
int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(index),
      ),
      bottomNavigationBar:
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            GestureDetector(
              onTap: () {
                setState(() {
                 index=0;
                 clindex1 = 0xFF9B3FB5;
                 cliconindex1=0xFFB53FA1;

                 cliconindex2 =0xFFB56E3F;
                 cliconindex3 =0xFFB56E3F;
                 clindex2=0;
                 clindex3=0;
                });
              },
                child: Container(
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      color: Color(clindex1),
                      borderRadius: BorderRadius.circular(50)
                  ),
                  child: Icon(Icons.account_box,color: Color(cliconindex1)),)),
            GestureDetector(
              onTap: () {
                setState(() {
                  index=1;
                  clindex2 = 0xFFB53F6E;
                  cliconindex2=0xFFB53FA1;
                  cliconindex3 =0xFFB56E3F;
                  cliconindex1=0xFFB56E3F;
                  clindex1=0;
                  clindex3=0;
                });
              },
                child: Container(
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      color: Color(clindex2),
                      borderRadius: BorderRadius.circular(50)
                  ),
                  child: Icon(Icons.home,color: Color(cliconindex2),),)),
            GestureDetector(
              onTap: () {
                setState(() {
                  clindex3 = 0xFF3F51B5;
                  cliconindex3=0xFFB53FA1;
                  cliconindex1=0xFFB56E3F;
                  cliconindex2 =0xFFB56E3F;
                  clindex2=0;
                  clindex1=0;
                  index=2;
                });
              },
                child: Container(
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    color: Color(clindex3),
                    borderRadius: BorderRadius.circular(50)
                  ),
                  child: Icon(Icons.settings,color: Color(cliconindex3)),)),
          ],
        )

    );
  }
}
