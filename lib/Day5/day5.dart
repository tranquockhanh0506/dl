import 'dart:async';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
class Day5 extends StatelessWidget {
  final String url;
  Completer<WebViewController> _controller = Completer<WebViewController>();

 Day5({Key? key, required this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

      ),
      body: WebView(

        initialUrl:url,
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController controller)
        {
          _controller.complete(controller);
        },
      ),
      // floatingActionButton: FutureBuilder<WebViewController>(
      //   future: _controller.future,
      //   builder: (BuildContext context ,
      //   AsyncSnapshot<WebViewController> controller)
      //   {
      //     if(controller.hasData)
      //       {
      //         return FloatingActionButton(
      //           child: Icon(Icons.ac_unit),
      //           onPressed: (){
      //             controller.data!.loadUrl('https://flutter.dev/?gclid=EAIaIQobChMIzOThuI_n8AIVAg4rCh3xMwqpEAAYASAAEgJYI_D_BwE&gclsrc=aw.ds');
      //           },
      //         );
      //       }
      //   },
      //
      //
      //
      // ),
    );
  }
}
