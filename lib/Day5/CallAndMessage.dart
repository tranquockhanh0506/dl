import 'package:flutter/material.dart';
import 'package:flutter_trainning/Day1/day1.dart';
import 'package:flutter_trainning/Day2/day2.dart';
import 'package:flutter_trainning/Day3/day3.dart';
import 'package:url_launcher/url_launcher.dart';
class CallAndMessage extends StatelessWidget {
  TextEditingController inputvalueController = TextEditingController();
  @override
  Widget build(BuildContext context) =>
      new Scaffold(
        appBar:  AppBar(


          title:  Text("Launchers"),
          actions: [
            Theme(
              data: Theme.of(context).copyWith(
                dividerColor: Colors.white,
                iconTheme: IconThemeData(color: Colors.white),
                textTheme: TextTheme().apply(bodyColor: Colors.white),
              ),
              child: PopupMenuButton<int>(
                icon: Icon(Icons.menu),
                color: Colors.indigo,
                onSelected: (item) => onSelected(context, item),
                itemBuilder: (context) => [
                  PopupMenuItem<int>(
                    value: 0,
                    child: Text('Settings'),
                  ),
                  PopupMenuItem<int>(
                    value: 1,
                    child: Text('Share'),
                  ),
                  PopupMenuDivider(),
                  PopupMenuItem<int>(
                    value: 2,
                    child: Row(
                      children: [
                        Icon(Icons.logout),
                        const SizedBox(width: 8),
                        Text('Sign Out'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        body:  SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextField(
                controller: inputvalueController,
              ),
              Card(
                color: Colors.white70,
                shape: RoundedRectangleBorder(
                  side: new BorderSide(color: Colors.white, width: 2.0),
                  borderRadius: BorderRadius.circular(25.0),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.all(30.0),
                        child: FlatButton.icon(
                          icon: Icon(Icons.add_to_home_screen),
                          label: Text(
                            "Open Website",
                            style: TextStyle(fontSize: 25),
                          ),
                          onPressed: () => launch("http://google.com"),
                        )
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10.0),
              Card(
                shape: RoundedRectangleBorder(
                  side: new BorderSide(color: Colors.white, width: 2.0),
                  borderRadius: BorderRadius.circular(25.0),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(30.0),
                      child: FlatButton.icon(
                        icon: Icon(Icons.call),
                        label:Text(
                          "Make a Call", style: TextStyle(fontSize: 25),
                        ),
                        onPressed: () => launch("tel://${inputvalueController.text}"),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10.0),
              Card(
                color: Colors.white70,
                shape: RoundedRectangleBorder(
                  side: new BorderSide(color: Colors.white, width: 2.0),
                  borderRadius: BorderRadius.circular(25.0),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(30.0),
                      child: FlatButton.icon(
                        icon: Icon(Icons.email),
                        label:Text(
                            "Send a Email", style: TextStyle(fontSize: 25)),
                        onPressed: () =>
                            launch(
                                "mailto:abhi@androidcoding.in?subject=Hi&body=How are you%20plugin"),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10.0),
              Card(
                shape: RoundedRectangleBorder(
                  side: new BorderSide(color: Colors.white, width: 2.0),
                  borderRadius: BorderRadius.circular(25.0),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(30.0),
                      child:  FlatButton.icon(
                        icon: Icon(Icons.sms),
                        label:Text(
                            "Write a SMS", style: TextStyle(fontSize: 25)),
                        onPressed: () => launch("sms:${inputvalueController.text}"),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )

        ,

      );
}

void onSelected(BuildContext context, int item) {
  switch (item) {
    case 0:
      Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => Day3()),
      );
      break;
    case 1:
      Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => Day2()),
      );
      break;
    case 2:
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => Day1()),
            (route) => false,
      );
  }
}
