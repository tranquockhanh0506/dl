import 'package:flutter/material.dart';

class Day4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Day 4'),
),
      body: const Center(
        child: MyStatefulWidget(),
      ),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  double sideLength = 50;
  final colorred = Colors.red;
  _showDialog() {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          insetPadding: EdgeInsets.all(25.0),
          content: Center(child: Text('asd')),
        );
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Image.network('https://picsum.photos/250?image=9'),
        ),
        SizedBox(
          height: 20,
        ),
        AnimatedContainer(
          height: sideLength,
          width: sideLength,
          duration: const Duration(seconds: 2),
          curve: Curves.easeIn,
          child: Material(
            color: Colors.yellow,
            child: InkWell(
              onTap: () {
                setState(() {
                  sideLength == 50 ? sideLength = 100 : sideLength = 50;
                });
              },
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          'Khanh techvify ',
          style: TextStyle().copyWith(color: colorred),
        ),
        TextButton(
            onPressed: () {},
            child: Container(
              padding: EdgeInsets.all(10.0),
              child: Text(
                'click',
                style: TextStyle().copyWith(color: colorred),
              ),
              decoration: BoxDecoration(
                  color: Colors.amber,
                  borderRadius: BorderRadius.circular(15.0)),
            )),
        SizedBox(height: 20,),
        TextButton(
            onPressed: (
                ) {
              _showDialog();
            },
            child: Container(
              padding: EdgeInsets.all(10.0),
              child: Text(
                'Show dialog',
                style: TextStyle().copyWith(color: colorred),
              ),
              decoration: BoxDecoration(
                  color: Colors.amber,
                  borderRadius: BorderRadius.circular(15.0)),
            )),
      ],
    );
  }
}
