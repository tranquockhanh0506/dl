import 'package:flutter/material.dart';
class Hinh4cham1 extends StatefulWidget {


  @override
  _Hinh4cham1State createState() => _Hinh4cham1State();
}

class _Hinh4cham1State extends State<Hinh4cham1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Hình 4.1'),

      ),
      body: Container(
        child: Column(
          children: [
            Expanded(
                flex: 8,
                child: Container(
                  alignment: Alignment.center,
                  child: Text('8'),
                  color: Colors.red,
                )),
            Expanded(
                flex: 5,
                child: Container(
                  color: Colors.blue,
                  child: Row(
                    children: [
                      Expanded(
                        flex: 7,
                        child: Container(
                          alignment: Alignment.center,
                          child: Text('5'),
                          color: Colors.indigo,),
                      ),
                      Expanded(
                        flex: 3,
                        child: Container(
                          child: Column(
                            children: [
                              Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: Row(
                                      children: [
                                        Expanded(
                                            flex: 1,
                                            child: Container(
                                              child: Column(
                                                children: [
                                                  Expanded(
                                                      flex: 1,
                                                      child: Container(
                                                        alignment: Alignment.center,
                                                        width: double.infinity,
                                                        color: Colors.green,
                                                        child: Text('1'),)),
                                                  Expanded(
                                                      flex: 1,
                                                      child: Container(
                                                        alignment: Alignment.center,
                                                        width: double.infinity,
                                                        color: Colors.blue,
                                                        child: Text('1'),)),
                                                ],
                                              ),)),
                                        Expanded(
                                            flex: 2,
                                            child: Container(
                                              alignment: Alignment.center,
                                              color: Colors.black54,
                                              child: Text('2'),))
                                      ],
                                    ),
                                  )),
                              Expanded(
                                  flex: 1,
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Text('3'),
                                    color: Colors.purpleAccent,
                                  ))
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
