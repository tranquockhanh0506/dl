import 'package:flutter/material.dart';
class Day7past2 extends StatefulWidget {


  @override
  _Day7past2State createState() => _Day7past2State();
}

class _Day7past2State extends State<Day7past2> {
  final PageController controller = PageController(initialPage: 0);
  _handelChangeselect(int index)
  {
    setState(() {
      _tabs.forEach((element) {
        element.tabHeight=0.0;
        element.isRadiusBoderBotomL=false;
        element.isRadiusBoderBotomR=false;
      });
    });
    _tabs[index].tabHeight =10.0;
    _colorChange = _tabs[index].isColors!;
    if(index == 0)
      {
        _tabs[index+1].isRadiusBoderBotomL = true;
      }else if(index == _tabs.length-1)
        {
          _tabs[index -1].isRadiusBoderBotomR = true;
        }else
          {
            _tabs[index -1].isRadiusBoderBotomR= true;
            _tabs[index +1].isRadiusBoderBotomL = true;
          }
  }
  _onPageViewChange(int index) {
    _handelChangeselect(index);
  }
  Color _colorChange = Colors.red;
  List<TAbModeL> _tabs =[];
  @override
  void initState() {
    // TODO: implement initState
    _tabs.add(TAbModeL(tabName: 'Top',tabHeight: 10.0,isColors: Colors.red));
    _tabs.add(TAbModeL(tabName: 'Entertain',isColors: Colors.green));
    _tabs.add(TAbModeL(tabName: 'Sports',isColors: Colors.blue));
    _tabs.add(TAbModeL(tabName: 'LifetStyle',isColors: Colors.orangeAccent));
    _tabs.add(TAbModeL(tabName: 'U.S',isColors: Colors.purple));

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        color: Colors.black,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Icon(
                    Icons.settings_backup_restore_sharp,
                    color: Colors.white,
                  ),
                  Text(
                    'SmartNews',
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  Icon(
                    Icons.settings,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
            SizedBox(height: 50,),
            Container(
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 80,
                    color: _colorChange,
                  ),
                  Positioned(
                    bottom: 10,
                      child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 80,
                    child: _BuildTab(context),
                  ))
                ],
              ),
            ),
            Expanded(
              child: PageView(
                onPageChanged: _onPageViewChange,
                scrollDirection: Axis.horizontal,
                controller: controller,
                children: <Widget>[
                  Container(
                    color: Colors.red.shade300,
                    child: Center(
                      child: Text('1st Page'),
                    ),
                  ),
                  Container(
                    color: Colors.green.shade300,
                    child: Center(
                      child: Text('2nd Page'),
                    ),
                  ),
                  Container(
                    color: Colors.blue.shade300,
                    child: Center(
                      child: Text('3rd Page'),
                    ),
                  ),
                  Container(
                    color: Colors.yellow.shade300,
                    child: Center(
                      child: Text('4th Page'),
                    ),
                  ),
                  Container(
                    color: Colors.purple.shade300,
                    child: Center(
                      child: Text('5th Page'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  Widget _BuildTab(BuildContext context)
  {
    return Row(
      children:_tabs.map((e) => _BuildTabModel(context, e, _tabs.indexOf(e))).toList(),
    );
  }
  Widget _BuildTabModel(BuildContext context , TAbModeL model , int index)
  {
    return Expanded(child: InkWell(
      onTap: ()
      {
        controller.jumpToPage(index);
        _handelChangeselect(index);
      },
      child: AnimatedContainer(
        duration: Duration(milliseconds: 400),
        alignment: Alignment.center,
        child: Text('${model.tabName}',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17,color: Colors.white),),
        decoration: BoxDecoration(
          color: model.isColors,
          borderRadius:BorderRadius.only(topRight: Radius.circular(8),
          topLeft: Radius.circular(8),
            bottomRight: Radius.circular(_changgeBoderBotom(model.isRadiusBoderBotomR) ?? 0.0),
            bottomLeft: Radius.circular(_changgeBoderBotom(model.isRadiusBoderBotomL) ??0.0)
          )
        ),
        height: 70+(model.tabHeight ?? 0.0),
      ) ,
    ));
  }
  double? _changgeBoderBotom(bool isradius)
  {
    if(isradius == false) return 0.0;
    return isradius ? 8.0 :0.0;
  }
}

class TAbModeL {
  String? tabName;
  double? tabHeight;
  bool isRadiusBoderBotomR;
  bool isRadiusBoderBotomL;
  Color? isColors;
TAbModeL({
   this.tabName ,
  this.tabHeight,
  this.isRadiusBoderBotomR=false,
  this.isRadiusBoderBotomL=false,
  this.isColors
});
}
