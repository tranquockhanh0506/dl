import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Day7 extends StatefulWidget {
  @override
  _Day7State createState() => _Day7State();
}

class _Day7State extends State<Day7> {

  final PageController controller = PageController(initialPage: 0);
  Color colos = Color(0xFFE91E63);
  _handlePageChange(int index) {
    setState(() {
      _tabs.forEach((element) {
        element.isRadiusBottomL = false;
        element.isRadiusBottomR = false;
        element.height = 0.0;
        element.isSelected = false;
      });
      _tabs[index].height = 10.0;
      _selectedColor = _tabs[index].color;
      _tabs[index].isSelected = true;
      // 1st
      if (index == 0) {
        _tabs[index + 1].isRadiusBottomL = true;
        // Last
      } else if (index == (_tabs.length - 1)) {
        _tabs[index - 1].isRadiusBottomR = true;
        // Con lai
      } else {
        _tabs[index - 1].isRadiusBottomR = true;
        _tabs[index + 1].isRadiusBottomL = true;
      }
    });
  }

  _onPageViewChange(int index) {
    print("Current Page: " + index.toString());
    _handlePageChange(index);
  }

  List<TabModel> _tabs = [];
  Color? _selectedColor = Colors.red;

  @override
  void initState() {
    _tabs.add(TabModel(
        tabNm: 'Top', isSelected: true, color: Colors.red, height: 10.0));
    _tabs.add(TabModel(
        tabNm: 'Entertain',
        isSelected: false,
        color: Colors.green,
        isRadiusBottomL: true));
    _tabs.add(TabModel(tabNm: 'Sports', isSelected: false, color: Colors.blue));
    _tabs.add(
        TabModel(tabNm: 'Lifestyle', isSelected: false, color: Colors.yellow));
    _tabs.add(TabModel(tabNm: 'U.S', isSelected: false, color: Colors.purple));
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: Container(
          color: Colors.black,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(
                      Icons.settings_backup_restore_sharp,
                      color: Colors.white,
                    ),
                    Text(
                      'SmartNews',
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    Icon(
                      Icons.settings,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Stack(
                  clipBehavior: Clip.none,
                  children: [
                    Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 60,
                          color: Colors.transparent,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 20,
                          color: _selectedColor,
                        ),
                      ],
                    ),
                    Positioned(
                        child: Container(
                            height: 80,
                            width: MediaQuery.of(context).size.width,
                            child: _buildTabs(context))),
                  ],
                ),
              ),
              Expanded(
                child: PageView(
                  onPageChanged: _onPageViewChange,
                  scrollDirection: Axis.horizontal,
                  controller: controller,
                  children: <Widget>[
                    Container(
                      color: Colors.red.shade300,
                      child: Center(
                        child: Text('1st Page'),
                      ),
                    ),
                    Container(
                      color: Colors.green.shade300,
                      child: Center(
                        child: Text('2nd Page'),
                      ),
                    ),
                    Container(
                      color: Colors.blue.shade300,
                      child: Center(
                        child: Text('3rd Page'),
                      ),
                    ),
                    Container(
                      color: Colors.yellow.shade300,
                      child: Center(
                        child: Text('4th Page'),
                      ),
                    ),
                    Container(
                      color: Colors.purple.shade300,
                      child: Center(
                        child: Text('5th Page'),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  Widget _buildTabs(BuildContext context) {
    return Row(
      children: _tabs
          .map((b) => _buildItemTab(context, b, _tabs.indexOf(b)))
          .toList(),
    );
  }

  Widget _buildItemTab(BuildContext context, TabModel model, int index) =>
      Expanded(
        child: InkWell(
          onTap: () {
            controller.jumpToPage(index);
            _handlePageChange(index);
          },
          child: AnimatedContainer(
            duration: Duration(milliseconds: 300),
            child: Text(
              '${model.tabNm}',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                  color: Colors.white),
            ),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: model.color,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(8),
                topLeft: Radius.circular(8),
                bottomRight: Radius.circular(
                    _displayRadius(model.isRadiusBottomR) ?? 0.0),
                bottomLeft: Radius.circular(
                    _displayRadius(model.isRadiusBottomL) ?? 0.0),
              ),
            ),
            height: 70.0 + (model.height ?? 0.0),
          ),
        ),
      );

  double? _displayRadius(bool isRadius) {
    if (isRadius == false) return 0.0;
    return isRadius ? 8.0 : 0.0;
  }

}

class TabModel {
  String? tabNm;
  bool isSelected;
  Color? color;
  double? height;
  bool isRadiusBottomR;
  bool isRadiusBottomL;

  TabModel(
      {this.tabNm,
      this.isSelected = false,
      this.color,
      this.height,
      this.isRadiusBottomR = false,
      this.isRadiusBottomL = false});
}
