import 'package:flutter/material.dart';
import 'package:flutter_trainning/Day1/day1.dart';
class Day2 extends StatefulWidget {

  @override
  _Day2State createState() => _Day2State();
}

class _Day2State extends State<Day2> {
  @override
  void initState() {
    super.initState();
    print('init state');
  }
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("đichanggeDêpndencise");
  }
  @override
  void didUpdateWidget(Day2 oldWidget) {
    super.didUpdateWidget(oldWidget);
    print('didUpdateWidget');
    // hàm setState được sử dụng trong hàm didUpdateWidget sẽ bị bỏ qua.
  }


  @override//ui sẽ được render trong hàm này
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('asfd'),
      ),
      body: Column(

       children: [
         Row(
           mainAxisAlignment: MainAxisAlignment.spaceBetween,
           children: [
             Text('1'),
             Text('2'),
             Text('3'),
             Text('4'),
           ],
         ),
         //
         TextButton(
           onPressed: (){

             Navigator.push(
               context,
               MaterialPageRoute(builder: (context) => Day1()),
             );
           },child: Text('click me !'),
         ),
         Container(
           width: 50,
           height: 50,
           margin: const EdgeInsets.all(15.0),
           padding: const EdgeInsets.all(3.0),
           decoration: BoxDecoration(
             color: Colors.red,
               borderRadius: BorderRadius.circular(50),
               border: Border.all(color: Colors.blueAccent)
           ),
           child: Text(''),
         )
       ],


      ),
    );
  }
  @override
  void deactivate() {
    // TODO: implement deactivate
    //Hàm này được gọi khi State bị gỡ khỏi cây widget nhưng nó có
    // thể xác nhận lại trước khi quá trình xoá kết thúc.
    super.deactivate();
    print('deactivate');
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    print('dispose');
    // Hàm này được gọi khi State bị gỡ ngay lập tức khỏi cây widget
    // và khi đó State không bao giờ được build trở lại.
  }
}
