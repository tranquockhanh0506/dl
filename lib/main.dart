import 'package:flutter/material.dart';
import 'package:flutter_trainning/Day2/day2.dart';
import 'package:flutter_trainning/Day3/nextscreen.dart';
import 'package:flutter_trainning/Day4/day4.dart';

import 'Day1/day1.dart';
import 'Day3/day3.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Day4/hinh4cham1.dart';
import 'Day5/ButtonNavigation.dart';
import 'Day5/CallAndMessage.dart';
import 'Day5/TabBarAppBar.dart';
import 'Day5/Webview.dart';
import 'Day5/day5.dart';
import 'Day7/Day7past2.dart';
import 'Day7/day7.dart';
import 'Day7/pageview.dart';
void main()  async {
  // WidgetsFlutterBinding.ensureInitialized();
  // SharedPreferences prefs = await SharedPreferences.getInstance();
  // var email = prefs.getString('email');
  // print(email);
  // runApp(MaterialApp(home: email == null ? Login() : Home()));
  runApp(MyApp());
}
class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: TextButton(
          onPressed: () async {
            //after the login REST api call && response code ==200
            SharedPreferences prefs = await SharedPreferences.getInstance();
            prefs.setString('email', 'tranquockhanh0506@gmail.com');
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (BuildContext ctx) => Home()));
          },
          child: Text('Login'),
        ),
      ),
    );
  }
}
class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Center(
        child: TextButton(
          onPressed: () async {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            prefs.remove('email');
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (BuildContext ctx) => Login()));
          },
          child: Text('Logout'),
        ),
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Day7past2(),
    );
  }
}

