import 'package:flutter/material.dart';
import 'package:flutter_trainning/Day3/nextscreen.dart';
import 'package:shared_preferences/shared_preferences.dart';
class Day3 extends StatefulWidget {


  @override
  _Day3State createState() => _Day3State();
}

class _Day3State extends State<Day3> {
  TextEditingController name =  TextEditingController();
  TextEditingController pass =  TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Day 3'),
      ),
      body: Container(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              InputForm('Họ tên','Nhập tên', true, name, 'Chưa nhập tên'),
              InputForm('Mật khẩu','Nhập mật khẩu', true, pass, 'Chưa nhập mật khẩu'),
              TextButton(onPressed: () {
                if (_formKey.currentState!.validate()) {
                  setState(()async {
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    prefs.setString('$name', '$pass');
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (BuildContext ctx) => NextScreen(name: name.text,pass: pass.text,)));
                  });
                  ScaffoldMessenger.of(context)
                      .showSnackBar(SnackBar(content: Text('')));
                }

              }, child: Text('Login'))
            ],
          ),
        ),
      ),
    );
  }
}
class InputForm extends StatelessWidget {
  final String title;
  final String hindetext;
  final bool required;
  final TextEditingController controler;
  final String errortext;

  const InputForm(this.title, this.hindetext, this.required, this.controler,
      this.errortext);

  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text.rich(TextSpan(children: [
                TextSpan(
                    text: title,
                    style:
                    TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
                TextSpan(
                    text: required ? ' *' : '',
                    style: TextStyle(color: Colors.red, fontSize: 16.0))
              ])),
            ],
          ),
          SizedBox(
            height: 8.0,
          ),
          TextFormField(
            validator: (value) {
              if (value == null || value.isEmpty) {
                return errortext;
              }
              return null;
            },
            controller: controler,
            decoration: new InputDecoration(
                border: new OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(15.0),
                  ),
                ),
                filled: true,
                hintStyle: new TextStyle(color: Colors.grey[800]),
                hintText: hindetext,
                fillColor: Colors.white70),
          )
        ],
      ),
    );
  }
}