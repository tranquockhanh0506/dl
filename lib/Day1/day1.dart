import 'package:flutter/material.dart';

class Day1 extends StatefulWidget {
  @override
  _Day1State createState() => _Day1State();
}

class _Day1State extends State<Day1> {
  SingingCharacter? _character = SingingCharacter.lafayette;
  TextEditingController _nameController = TextEditingController();
  TextEditingController _ageController = TextEditingController();
  TextEditingController _addressController = TextEditingController();
  String gender="Nam";
  bool isShowing = false;
  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    _nameController.dispose();
    _ageController.dispose();
    _addressController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Demo Day 1'),
        ),
        body: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InputForm('Tên', 'Vui lòng nhập tên', true, _nameController,
                    'Tên chưa được nhập'),
                InputForm('Tuổi', 'Vui lòng nhập tuổi', true, _ageController,
                    'Tuổi chưa được nhập'),
                InputForm('Địa chỉ', 'Vui lòng nhập địa chỉ', true,
                    _addressController, ' Địa chỉ chưa được nhập'),
              Row(
                children: [
                  Expanded(
                    child: RadioListTile<SingingCharacter>(
                      title: const Text('Nam'),
                      value: SingingCharacter.lafayette,
                      groupValue: _character,
                      onChanged: (SingingCharacter? value) {
                        setState(() {
                          gender="Nam";
                          _character = value;
                        });
                      },
                    ),
                  ),
                  Expanded(
                    child: RadioListTile<SingingCharacter>(
                      title: const Text('Nữ'),
                      value: SingingCharacter.jefferson,
                      groupValue: _character,
                      onChanged: (SingingCharacter? value) {
                        setState(() {
                          gender="Nữ";
                          _character = value;
                        });
                      },
                    ),
                  ),
                ],
              ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            setState(() {
                              isShowing = true;
                              print(_nameController);
                              print(isShowing);
                            });
                            ScaffoldMessenger.of(context)
                                .showSnackBar(SnackBar(content: Text('')));
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.all(10.0),
                          child: Text(
                            'Confirm',
                            style: TextStyle(color: Colors.red),
                          ),
                          decoration: BoxDecoration(
                              color: Colors.amber,
                              borderRadius: BorderRadius.circular(15.0)),
                        )),
                    TextButton(
                        onPressed: () {
                            setState(() {
                             _nameController.text="";
                             _addressController.text="";
                             _ageController.text="";

                            });
                            // ScaffoldMessenger.of(context)
                            //     .showSnackBar(SnackBar(content: Text('')));

                        },
                        child: Container(
                          padding: EdgeInsets.all(10.0),
                          child: Text(
                            'delete data',
                            style: TextStyle(color: Colors.red),
                          ),
                          decoration: BoxDecoration(
                              color: Colors.amber,
                              borderRadius: BorderRadius.circular(15.0)),
                        )),

                  ],
                ),
                isShowing != true
                    ? Container()
                    : Container(
                        child: Padding(
                          padding: EdgeInsets.all(16.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Tên : ${_nameController.text}',
                                style: TextStyle(
                                    fontSize: 16.0, color: Colors.blue),
                              ),
                              SizedBox(
                                height: 9.0,
                              ),
                              Text('Tuổi : ${_ageController.text}',
                                  style: TextStyle(
                                      fontSize: 16.0, color: Colors.blue)),
                              SizedBox(
                                height: 9.0,
                              ),
                              Text('Địa chỉ : ${_addressController.text}',
                                  style: TextStyle(
                                      fontSize: 16.0, color: Colors.blue)),
                              Text('Giới tính : ${gender}',
                                  style: TextStyle(
                                      fontSize: 16.0, color: Colors.blue)),


                            ],
                          ),
                        ),
                      ),
              ],
            ),
          ),
        ));
  }
}

class InputForm extends StatefulWidget {
  final String title;
  final String hindetext;
  final bool required;

  final TextEditingController controler;
  final String errortext;

  const InputForm(this.title, this.hindetext, this.required, this.controler,
      this.errortext);

  @override
  _InputFormState createState() => _InputFormState();
}

class _InputFormState extends State<InputForm> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text.rich(TextSpan(children: [
                TextSpan(
                    text: widget.title,
                    style:
                        TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
                TextSpan(
                    text: widget.required ? ' *' : '',
                    style: TextStyle(color: Colors.red, fontSize: 16.0))
              ])),
            ],
          ),
          SizedBox(
            height: 8.0,
          ),
          TextFormField(
            validator: (value) {
              if (value == null || value.isEmpty) {
                return widget.errortext;
              }
              return null;
            },
            controller: widget.controler,
            decoration: new InputDecoration(
                border: new OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(15.0),
                  ),
                ),
                filled: true,
                hintStyle: new TextStyle(color: Colors.grey[800]),
                hintText: widget.hindetext,
                fillColor: Colors.white70),
          )
        ],
      ),
    );
  }
}
enum SingingCharacter { lafayette, jefferson }
